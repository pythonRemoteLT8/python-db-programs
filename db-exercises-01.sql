create DATABASE IF NOT EXISTS new_db;
use new_db;

  CREATE TABLE IF NOT EXISTS elements (
      v integer not null
  );

  TRUNCATE elements;

insert into elements (v) VALUES (2);
insert into elements (v) VALUES (10);
insert into elements (v) VALUES (10);
insert into elements (v) VALUES (20);

  CREATE TABLE IF NOT EXISTS events (
      event_type integer not null,
      value integer not null,
      time timestamp not null,
      unique(event_type, time)
  );

  TRUNCATE events;

insert into events values(2, 5, '2015-05-09 12:42:00');
insert into events values(4, -42, '2015-05-09 13:19:57');
insert into events values(2, 2, '2015-05-09 14:48:30');
insert into events values(2, 7, '2015-05-09 12:54:39');
insert into events values(3, 16, '2015-05-09 13:19:57');
insert into events values(3, 20, '2015-05-09 15:01:09');

/*
write an SQL query that, for each event_type that has been registered more than once, returns the
difference between the latest (i.e. the most recent in terms of time) and the second latest value.
The table should be ordered by event_type (in ascending order).

For example, given the following data:
   event_type | value      | time
  ------------+------------+--------------------
   2          | 5          | 2015-05-09 12:42:00
   4          | -42        | 2015-05-09 13:19:57
   2          | 2          | 2015-05-09 14:48:30
   2          | 7          | 2015-05-09 12:54:39
   3          | 16         | 2015-05-09 13:19:57
   3          | 20         | 2015-05-09 15:01:09

your query should return the following rowset:
   event_type | value
  ------------+-----------
   2          | -5
   3          | 4

For the event_type 2, the latest value is 2 and the second latest value is 7, so the difference between them is −5.

The names of the columns in the rowset don't matter, but their order does.
*/

SELECT * FROM ( SELECT * FROM (SELECT * FROM events ORDER BY time DESC LIMIT 2 ) AS min_2 ORDER BY time ASC LIMIT 1 ) event_type ;

SELECT MAX(time) FROM events;

select e.event_type, max(e.time)-min(e.time) from events e group by e.event_type having count(e.event_type)>1 order by event_type;

select e.event_type, max(e.value)-min(e.value) from events e group by e.event_type having count(e.event_type)>1 order by event_type;


SELECT ee.event_type, e.time FROM events e,
( SELECT e.event_type, MIN(e.time) AS min_time, MAX(e.time) AS max_time FROM events e
 GROUP BY e.event_type HAVING COUNT(e.event_type)>1 ) ee
WHERE ee.event_type = e.event_type ORDER BY e.event_type, e.time DESC;

SELECT ee.event_type, e.time FROM events e,
(SELECT e.event_type, MIN(e.time) AS min_time, MAX(e.time) AS max_time FROM events e
 GROUP BY e.event_type HAVING COUNT(e.event_type)>1 ) ee
WHERE ee.event_type = e.event_type AND e.time = ee.min_time OR e.time = ee.max_time
ORDER BY e.event_type, e.time ;

 SELECT DISTINCT limited.event_type, eee.time FROM (
 SELECT ee.event_type, e.time FROM events e,
( SELECT e.event_type, MIN(e.time) AS min_time, MAX(e.time) AS max_time FROM events e
 GROUP BY e.event_type HAVING COUNT(e.event_type)>1 ) ee
WHERE ee.event_type = e.event_type
ORDER BY e.event_type, e.time LIMIT 1,100 ) limited, events eee
WHERE limited.event_type = eee.event_type

 SELECT DISTINCT limited.event_type, eee.time, eee.value FROM (
 SELECT ee.event_type, e.time FROM events e,
( SELECT e.event_type, MIN(e.time) AS min_time, MAX(e.time) AS max_time FROM events e
 GROUP BY e.event_type HAVING COUNT(e.event_type)>1 ) ee
WHERE ee.event_type = e.event_type
ORDER BY e.event_type, e.time LIMIT 1,100 ) limited, events eee
WHERE limited.event_type = eee.event_type
ORDER BY event_type, eee.time DESC LIMIT 1,100

