from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError, PendingRollbackError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import sessionmaker
import csv

user = 'root'
password = '1My_sql!SQL'

eng = create_engine('mysql://' + user + ':' + password + '@localhost:3306/cinematic')
# print( eng )
# help( eng )

base = declarative_base()

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45), nullable=False)
    surname = Column(String(45), nullable=False)
    rating = Column(Integer, nullable=False)

    # Custom (generic) constructor
    # REF: https://www.digitalocean.com/community/tutorials/how-to-use-args-and-kwargs-in-python-3
    def __init__(self, *args, **kwargs):
        try:
            for key, value in kwargs.items():

                # args -- tuple of anonymous arguments
                # kwargs -- dictionary of named arguments
                self.name = kwargs.get('name', value)
                self.surname = kwargs.get('surname', value)
                self.rating = kwargs.get('rating', value)

        except KeyError as error:
            print( "Not found in DB dict description %s" % error )

    def __init__(self, name, surname, rating):
        self.name = name
        self.surname = surname
        self.rating = rating

    def __repr__(self):
        return f"""<Directors: id={self.director_id}, name={self.name}, surname={self.surname}, rating={self.rating}>"""\
               + " OR as JSON:\n" + self.jsonize()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def jsonize(self):
        variables = [var for var in self.as_dict()]
        #fl = filter(lambda x: type( x ), variables );

        return "{" + ",".join([f"\"{var}\": \"{getattr(self, var)}\"" for var in variables]) + "}"

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(45), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(45), nullable=False)
    director_id = Column(Integer, nullable=False)
    rating = Column(Integer, nullable=False)

    # Custom constructor
    def __init__(self, db_dict):
        try:

            self.title = db_dict['title']
            self.year = db_dict['year']
            self.category = db_dict['category']
            self.director_id = db_dict['director_id']
            self.rating = db_dict['rating']

        except KeyError as error:
            print( "Not found in DB dict description %s" % error )

    def __repr__(self):
        return f"<Movies: movie_id={self.movie_id}, title={self.title}, year={self.year}, " \
               f"category={self.category}, director_id={self.director_id}, rating={self.rating}>"


directors = [{'name': 'Frank', 'surname': 'Darabont', 'rating': 7},
             {'name': 'Francis Ford', 'surname': 'Coppola', 'rating': 8},
             {'name': 'Quentin', 'surname': 'Tarantino', 'rating': 10},
             {'name': 'Christopher', 'surname': 'Nolan', 'rating': 9},
             {'name': 'David', 'surname': 'Fincher', 'rating': 7}]

movies = [{'title': 'The Shawshank Redemption', 'year': 1994, 'category': 'Drama', 'director_id': 1, 'rating': 8},
          {'title': 'The Green Mile', 'year': 1999, 'category': 'Drama', 'director_id': 1, 'rating': 6},
          {'title': 'The Godfather', 'year': 1972, 'category': 'Crime', 'director_id': 2, 'rating': 7},
          {'title': 'The Godfather III', 'year': 1990, 'category': 'Crime', 'director_id': 2, 'rating': 6},
          {'title': 'Pulp Fiction', 'year': 1994, 'category': 'Crime', 'director_id': 3, 'rating': 9},
          {'title': 'Inglourious Basterds', 'year': 2009, 'category': 'War', 'director_id': 3, 'rating': 8},
          {'title': 'The Dark Knight', 'year': 2008, 'category': 'Action', 'director_id': 4, 'rating': 9},
          {'title': 'Interstellar', 'year': 2014, 'category': 'Sci-fi', 'director_id': 4, 'rating': 8},
          {'title': 'The Prestige', 'year': 2006, 'category': 'Drama', 'director_id': 4, 'rating': 10},
          {'title': 'Fight Club', 'year': 1999, 'category': 'Drama', 'director_id': 5, 'rating': 7},
          {'title': 'Zodiac', 'year': 2007, 'category': 'Crime', 'director_id': 5, 'rating': 5},
          {'title': 'Seven', 'year': 1995, 'category': 'Drama', 'director_id': 5, 'rating': 8},
          {'title': 'Alien 3', 'year': 1992, 'category': 'Horror', 'director_id': 5, 'rating': 5}]

input_file = csv.DictReader(open("./cinematic-db-directors.csv", mode='r+t'))
dict_from_csv = csv.DictReader( input_file )

base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
session = Session()

for director in directors:
    print(director)
    rec_director = Directors(director['name'], director['surname'], director['rating'])
    session.add(rec_director)

try:
    session.commit()

# Exception handling for hardcoded input
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

session = Session()

# And now add just Lithuanian directors from the input file
for row in input_file:
    rec_director = Directors(name=row['name'],surname=row['surname'],rating=row['rating'])
    print( rec_director )
    session.add(rec_director)

try:
    session.commit()

# Exception handling for file input
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

session = Session()
for movie in movies:

    rec_movie = Movies(movie)
    print(rec_movie)
    session.add(rec_movie)

try:
    session.commit()

# Exception handling
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

for t in base.metadata.sorted_tables:
    print(t.name)

session = Session()
try:
    for directors in session.query(Directors).all():
        print(directors)

    for movies in session.query(Movies).all():
        print(movies)

except PendingRollbackError as roll_back_error:
    print("Program rollback failure: %s" % roll_back_error)