SELECT * FROM cars, bookings WHERE bookings.car_id = cars.car_id AND client_id = 5;

SELECT bookings.booking_id AS bookings_booking_id, bookings.client_id AS bookings_client_id, bookings.car_id AS bookings_car_id, bookings.start_date AS bookings_start_date, bookings.end_date AS bookings_end_date, bookings.total_amount AS bookings_total_amount
FROM bookings WHERE bookings.client_id = 5;

SELECT clients.client_id AS clients_client_id, clients.name AS clients_name, clients.surname AS clients_surname, clients.address AS clients_address, clients.city AS clients_city
FROM clients WHERE clients.client_id = 5;

SELECT cars.car_id, cars.producer, cars.model, cars.year, cars.horse_power, cars.price_per_day
FROM bookings LEFT OUTER JOIN cars ON bookings.car_id = cars.car_id
WHERE bookings.client_id = 5;

SELECT clients.client_id AS clients_client_id, bookings.total_amount, bookings.start_date AS bookings_total_amount
FROM clients INNER JOIN bookings ON clients.client_id = bookings.client_id WHERE bookings.total_amount > 1100;

SELECT clients.client_id AS clients_client_id, bookings.total_amount AS bookings_total_amount
FROM clients INNER JOIN bookings ON clients.client_id = bookings.client_id
WHERE bookings.total_amount > 1100 AND bookings.start_date >= '2020-07-09';


SELECT sum(bookings.total_amount) AS sum_1, clients.name AS clients_name, clients.surname AS clients_surname
FROM bookings INNER JOIN clients ON clients.client_id = bookings.client_id
WHERE bookings.start_date >= '2020-07-09' AND bookings.end_date <= '2020-07-17' GROUP BY clients.client_id;

 SELECT MAX(total_amount) FROM bookings WHERE start_date >= '2020-07-13' AND end_date <= '2020-08-02';

DROP PROCEDURE IF EXISTS `most_expensive`;

DELIMITER //
CREATE PROCEDURE most_expensive(IN data_start DATE, IN data_end DATE)
BEGIN

    DECLARE MESSAGE_TEXT VARCHAR(125);
    DECLARE MYSQL_ERRNO INT;

	DECLARE EXIT HANDLER FOR SQLSTATE '45000'
	BEGIN
		ROLLBACK;
		SELECT CONCAT('An error has occurred, operation rollbacked and the stored procedure was terminated: ',
        'The new date is earlier or same as ', data_start, ' than ', data_end ) FROM DUAL;
	END;

    IF TO_DAYS( data_end ) - TO_DAYS( data_start ) <= 0 THEN
		SET MESSAGE_TEXT = (SELECT CONCAT('The new date is earlier', data_end, ' than ', data_start ) AS message FROM DUAL), MYSQL_ERRNO = 1000;
    SIGNAL SQLSTATE '45000';

    END IF;

    SELECT MAX(total_amount) FROM bookings WHERE start_date >= data_start AND end_date <= data_end;
END//
DELIMITER ;

CALL most_expensive('2020-07-14', '2020-07-01');

START TRANSACTION;
DELETE FROM bookings WHERE client_id = 1;
DELETE FROM clients WHERE client_id = 1;
COMMIT;

ALTER TABLE bookings
DROP FOREIGN KEY `client_id_fk`,
DROP FOREIGN KEY `car_id_fk`;
ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id) ON DELETE CASCADE;

DELETE FROM clients WHERE client_id = 2;
