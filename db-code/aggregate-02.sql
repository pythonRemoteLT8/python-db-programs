
SELECT DISTINCT directors.rating, movies.title, directors.director_id, directors.name, directors.surname
FROM movies JOIN directors ON movies.director_id = directors.director_id
WHERE movies.category = 'Drama' AND directors.rating = 7;

SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.rating) AS avg_1
FROM directors JOIN movies ON directors.director_id = movies.director_id GROUP BY directors.director_id;

SELECT count(movies.movie_id) AS counter, directors.director_id AS ddirectorid,
  movies.movie_id AS mmoviesid, movies.title, directors.name AS ddname, directors.surname AS ddsurname, avg(movies.rating) AS average
    FROM directors
     JOIN movies ON directors.director_id = movies.director_id
       WHERE movies.year
         BETWEEN 2011 AND 2022
          GROUP BY ddirectorid, mmoviesid

SELECT directors.director_id, directors.name AS directors_name, directors.surname AS directors_surname
FROM directors INNER JOIN movies ON directors.director_id = movies.director_id
 AND movies.rating < 9;

SELECT * FROM movies WHERE year BETWEEN 2011 AND 2014 AND movies.rating < 9;

SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.rating) AS avg_1
FROM directors JOIN movies ON directors.director_id = movies.director_id WHERE movies.year
BETWEEN 1950 AND 2022 GROUP BY directors.director_id;


-- For constraints to be added:
ALTER TABLE `cinematic`.`directors` DROP CONSTRAINT uniq_director_idx;

ALTER TABLE `cinematic`.`directors`
ADD CONSTRAINT uniq_director_idx UNIQUE KEY(name,surname);

ALTER TABLE `cinematic`.`movies` DROP CONSTRAINT uniq_movies_idx;

ALTER TABLE `cinematic`.`movies`
ADD CONSTRAINT uniq_movies_idx UNIQUE KEY(title,year,category);

SET foreign_key_checks = 0;

DELETE FROM directors;
DELETE FROM movies;

SET foreign_key_checks = 1;

