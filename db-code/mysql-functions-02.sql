
DROP FUNCTION IF EXISTS `sum_by_date`;

DELIMITER $$
CREATE FUNCTION sum_by_date( passed_date VARCHAR(10) ) RETURNS DECIMAL(10,2) DETERMINISTIC
BEGIN

	DECLARE all_sum DECIMAL(10,2);

	-- CANDIDATE for stored procedure
	SELECT ROUND( SUM(b.total_amount), 2) INTO all_sum FROM clients c JOIN bookings b ON c.client_id = b.client_id
	JOIN cars r ON b.car_id = r.car_id
	WHERE b.start_date >= STR_TO_DATE( passed_date, '%Y-%m-%d' )
	AND b.end_date <= CURDATE()
    AND TO_DAYS(STR_TO_DATE( passed_date, '%Y-%m-%d' )) IS NOT NULL
	AND r.horse_power <= 120 ORDER BY b.total_amount DESC;

    RETURN( all_sum );

END$$
DELIMITER ;

commit;

SHOW FUNCTION STATUS WHERE db = 'car_rental';
SELECT sum_by_date('2020-07-01') as 'July 01',
sum_by_date('2020-07-10') as 'July_10',
sum_by_date('2020-07-16') as JULY_16,
sum_by_date('2020-02-31') as Err_date FROM DUAL;

SELECT TO_DAYS(STR_TO_DATE( '2020-02-31', '%Y-%m-%d' ) ) FROM DUAL;

-- Automated

DROP TRIGGER trigger_for_sums;
DELIMITER $$
CREATE TRIGGER trigger_for_sums BEFORE DELETE ON bookings FOR EACH ROW
BEGIN
 SET @all_sums = 0;

 -- OLD (the one that is in the table)
 -- NEW (the one that is about to be inserted/updated/deleted)
 -- Effectively bookings.start_date is passed here!
 --  '2020-07-14'
 CALL proc_sum_by_date( OLD.start_date, @all_sums);
END$$



