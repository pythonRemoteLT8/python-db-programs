
DROP PROCEDURE IF EXISTS set_old_prices;

DELIMITER //

CREATE PROCEDURE set_old_prices(IN data DATE)
BEGIN

  DECLARE CONTINUE HANDLER FOR SQLEXCEPTION
    SELECT 'SQLEXCEPTION handler was activated' AS msg;
      BEGIN -- inner block
        DECLARE CONTINUE HANDLER FOR 1292
          SELECT CONCAT('Error converting date (', data, ') occurred') AS message;
      END;

   START TRANSACTION;
	UPDATE cars SET price_per_day=price_per_day+100 WHERE year <= YEAR( data ) AND YEAR( data ) < 2019;
   COMMIT;

END//

DELIMITER ;

SELECT YEAR('2002-01-02') FROM DUAL;

CALL set_old_prices('2018-01-01');