DELIMITER $$
DROP FUNCTION IF EXISTS `check_for_discount`;

CREATE FUNCTION check_for_discount(client INT) RETURNS VARCHAR(30) DETERMINISTIC
	BEGIN

		DECLARE result VARCHAR(30);
		DECLARE reservations_count INT;
		DECLARE reservations_value INT;

    -- Assignment  :=
	SET reservations_count = (SELECT COUNT(booking_id) FROM bookings WHERE client_id = client);
	SET reservations_value = (SELECT SUM(total_amount) FROM bookings WHERE client_id = client);

	IF reservations_count >= 2 AND reservations_value >= 500 THEN
		SET result = 'ACCEPTED';
	ELSE
		SET result = 'DENIED';
	END IF;

	RETURN (result);
	END$$

DELIMITER ;

SELECT check_for_discount(client_id) FROM clients;

ALTER TABLE clients ADD column
	status VARCHAR(30);
commit;

DROP TABLE IF EXISTS client_status;
CREATE TABLE client_status (

	client_status_id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    client_id INTEGER,
	status VARCHAR(30)
    );

commit;

SELECT COUNT(booking_id) FROM bookings WHERE client_id = 4;
SELECT SUM(total_amount) FROM bookings WHERE client_id = 4;

-- INSERT using function SELECT
INSERT INTO client_status
   (client_id, status) SELECT client_id, check_for_discount(client_id) FROM clients;
commit;

-- UPDATE using function SELECT
UPDATE client_status SET status = (SELECT check_for_discount( 4 ) FROM clients where client_id = 4), client_id = client_id;
commit;

SELECT check_for_discount( 4 ) FROM clients where client_id = 4;

SELECT check_for_discount(client_id) FROM clients;
