SELECT * FROM cinematic.directors;

-- 4 Different but not distinct
SELECT directors.rating, directors.director_id, directors.name, directors.surname
FROM movies JOIN directors ON movies.director_id = directors.director_id
WHERE movies.category = 'Drama' AND directors.rating = 7;

-- 2 Different and distinct
SELECT DISTINCT directors.rating, directors.director_id, directors.name, directors.surname
FROM movies JOIN directors ON movies.director_id = directors.director_id
WHERE movies.category = 'Drama' AND directors.rating = 7;

SELECT directors.rating, movies.title, directors.director_id, directors.name, directors.surname
FROM movies JOIN directors ON movies.director_id = directors.director_id;
-- WHERE movies.category = 'Drama' AND directors.rating = 7;

SELECT directors.rating, movies.title, directors.director_id, directors.name, directors.surname
FROM movies LEFT JOIN directors ON movies.director_id = directors.director_id
WHERE movies.category = 'Drama' AND directors.rating = 7;

SELECT directors.rating, movies.title, directors.director_id, directors.name, directors.surname
FROM movies RIGHT JOIN directors ON movies.director_id = directors.director_id
WHERE movies.category = 'Drama' AND directors.rating = 7;

-- Let's count number of movies by director and retrieve the average for director's all movies
SELECT directors.director_id, directors.name, directors.surname, count(movies.movie_id) AS count_1, AVG(movies.rating) AS avg_1
FROM directors JOIN movies ON directors.director_id = movies.director_id
GROUP BY directors.director_id;

-- Let's get averages within year range
SELECT count(movies.movie_id) AS counter, directors.director_id AS ddirectorid, directors.name AS ddname, directors.surname AS
ddsurname, AVG(movies.rating) AS average
    FROM directors
     JOIN movies ON directors.director_id = movies.director_id
       WHERE movies.year
         BETWEEN 1990 AND 2022
          GROUP BY ddirectorid;

-- Let's group by average which is less than 7
SELECT directors.director_id, directors.name AS directors_name, directors.surname AS directors_surname
FROM directors INNER JOIN movies ON directors.director_id = movies.director_id
GROUP BY directors.director_id HAVING AVG(movies.rating) < 7;

-- Let's group by average which is more than 7
SELECT directors.director_id, directors.name AS directors_name, directors.surname AS directors_surname
FROM directors INNER JOIN movies ON directors.director_id = movies.director_id
GROUP BY directors.director_id HAVING AVG(movies.rating) >= 7;

-- Let's group by average which is more than 7
SELECT directors.director_id, directors.name AS directors_name, directors.surname AS directors_surname
FROM directors LEFT JOIN movies ON directors.director_id = movies.director_id
GROUP BY directors.director_id HAVING AVG(movies.rating) >= 7 OR AVG(directors.rating) >=7;

-- Add LT directors
SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.rating) AS avg_1
FROM directors RIGHT JOIN movies ON directors.director_id = movies.director_id WHERE movies.year
BETWEEN 1950 AND 2022 GROUP BY directors.director_id;

-- For constraints to be added:
ALTER TABLE `cinematic`.`movies` DROP CONSTRAINT uniq_movies_idx;

ALTER TABLE `cinematic`.`movies`
ADD CONSTRAINT uniq_movies_idx UNIQUE KEY(title,year,category);

ALTER TABLE `cinematic`.`directors` DROP CONSTRAINT uniq_director_idx;

ALTER TABLE `cinematic`.`directors`
ADD CONSTRAINT uniq_director_idx UNIQUE KEY(name,surname);
commit;

INSERT INTO `movies` (title,year,category, director_id) VALUES ('The Shawshank Redemption',2021,'Crime',1);
