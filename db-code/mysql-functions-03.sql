
DROP PROCEDURE IF EXISTS proc_sum_by_date;

DELIMITER $$
CREATE PROCEDURE proc_sum_by_date(IN passed_date VARCHAR(10), INOUT out_all_sum DECIMAL(10,2))

BEGIN

	-- CANDIDATE for stored procedure
	SELECT ROUND( SUM(b.total_amount), 2) INTO out_all_sum FROM clients c JOIN bookings b ON c.client_id = b.client_id
	JOIN cars r ON b.car_id = r.car_id
	WHERE b.start_date >= passed_date
	AND b.end_date <= CURDATE()
	AND r.horse_power <= 120 ORDER BY b.total_amount DESC;

END$$
DELIMITER ;

SET @all_sum_july = 0;
SET @all_sum_mid_july = 0;
CALL proc_sum_by_date('2020-07-01', @all_sum_july);
CALL proc_sum_by_date('2020-07-16', @all_sum_mid_july);

SELECT @all_sum_july AS all_july, @all_sum_mid_july AS mid_july FROM DUAL;

SET @all_sums = -1;

-- Check triggered result
SELECT @all_sums AS all_sums_triggered FROM DUAL;

 SET FOREIGN_KEY_CHECKS = 0;
-- Demo inserts
INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES (2, 4, '2020-07-14', '2020-07-18', 520);

 SET FOREIGN_KEY_CHECKS = 1;

DELETE FROM `car_rental`.`bookings` WHERE booking_id=27;
commit;
