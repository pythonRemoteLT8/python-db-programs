ALTER USER 'root'@'localhost' IDENTIFIED BY '1My_sql!SQL';
commit;

SELECT * FROM car_rental.bookings;

SELECT COUNT(b.client_id) FROM bookings AS b, cars AS c;

SELECT COUNT(*) FROM bookings AS b, cars AS c;

SELECT b.client_id, c.car_id FROM bookings AS b, cars AS c INNER JOIN bookings ON c.car_id = bookings.car_id;

SELECT b.client_id, c.producer FROM bookings AS b, cars AS c INNER JOIN bookings ON c.car_id = bookings.car_id;

SELECT b.client_id, c.producer FROM bookings AS b, cars AS c INNER JOIN bookings ON c.car_id = bookings.car_id;

SELECT b.client_id, c.car_id FROM bookings AS b, cars AS c LEFT JOIN bookings ON c.car_id = bookings.car_id;

SELECT b.client_id, c.car_id FROM cars AS c, bookings AS b LEFT JOIN cars ON cars.car_id = b.car_id;

SELECT b.client_id, c.car_id FROM bookings AS b, cars AS c RIGHT JOIN bookings ON c.car_id = bookings.car_id;

SELECT b.client_id, c.car_id FROM cars AS c, bookings AS b RIGHT JOIN cars ON cars.car_id = b.car_id;

