DROP TRIGGER IF EXISTS `insert_new_rating`;

DELIMITER //
CREATE TRIGGER insert_new_rating BEFORE insert ON movies
FOR EACH ROW
 BEGIN

 if NEW.rating is null then
    SET new.rating = (
		select avg(movies.rating)
        FROM directors
		JOIN movies ON directors.director_id = movies.director_id
        where directors.director_id = new.director_id
        );

    SET @message_text = CONCAT( 'The new rating is ', NEW.rating, ' as default ');
    SIGNAL SQLSTATE '01000' SET MESSAGE_TEXT = @message_text,
    MYSQL_ERRNO = 1000;

 END IF;
END;//



/* -- Find the older movies for EU bienale retrospective

SELECT directors.name,
directors.surname,
count(movies.movie_id) AS count_1,
avg(movies.rating) AS avg_1
FROM directors
JOIN movies ON directors.director_id = movies.director_id
WHERE movies.year BETWEEN 1950 AND 1980
GROUP BY directors.director_id
having count(movies.movie_id)>1;

*/