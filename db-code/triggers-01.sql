DROP FUNCTION IF EXISTS `check_for_discount`;

DELIMITER //
CREATE FUNCTION check_for_discount(client INT)
RETURNS VARCHAR(30)

DETERMINISTIC
READS SQL DATA

BEGIN
DECLARE result VARCHAR(30);
DECLARE reservations_count INT;
DECLARE reservations_value INT;

SET reservations_count = (SELECT COUNT(booking_id) FROM bookings WHERE client_id = client);
SET reservations_value = (SELECT SUM(total_amount) FROM bookings WHERE client_id = client);

IF reservations_count >= 2 AND reservations_value >= 500 THEN
    SET result = 'ACCEPTED';
ELSE
    SET result = 'DENIED';
END IF;
RETURN (result);
END//

DELIMITER ;

SELECT client_id, name, surname, check_for_discount(client_id) FROM clients;

SELECT D.client_id, name, surname, D.discount FROM clients,
(SELECT clients.client_id, check_for_discount(client_id) as discount FROM clients) D
WHERE D.discount = 'D' AND D.client_id = clients.client_id;

SELECT DATEDIFF('2019-02-01','2019-03-01') FROM DUAL;
SELECT DATEDIFF('2000-02-01','2000-03-01') FROM DUAL;

DROP TRIGGER IF EXISTS `calculate_amount`;
CREATE TRIGGER calculate_amount BEFORE INSERT ON bookings FOR EACH ROW
SET NEW.total_amount = (SELECT price_per_day FROM cars c WHERE c.car_id = NEW.car_id) * DATEDIFF(NEW.end_date, NEW.start_date);

-- 2320 EUR
SELECT start_date,end_date,booking_id, total_amount FROM bookings WHERE booking_id = 18;

-- 2240 EUR
UPDATE `car_rental`.`bookings` SET start_date= '2019-02-01', end_date='2019-03-01' WHERE booking_id =18;

DROP TRIGGER IF EXISTS `update_calculated_amount`;

DELIMITER //
CREATE TRIGGER update_calculated_amount BEFORE UPDATE ON bookings
FOR EACH ROW
 BEGIN

 IF NEW.total_amount <= old.total_amount THEN
    SET @message_text = CONCAT( 'The new total amount calculated is ', NEW.total_amount, ' less or same as ', OLD.total_amount);
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
 END IF;
END;//

DELIMITER ;

-- 2240 EUR
UPDATE `car_rental`.`bookings` SET start_date= '2018-02-01', end_date='2018-02-03' WHERE booking_id =18;

SET MSG = (SELECT CONCAT('The new total amount is ', '2', ' less than ', '3') AS message FROM DUAL);
