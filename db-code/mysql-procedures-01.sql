	-- CANDIDATE for stored procedure
	SELECT ROUND( SUM(b.total_amount), 2) FROM clients c JOIN bookings b ON c.client_id = b.client_id
	JOIN cars r ON b.car_id = r.car_id
	WHERE b.start_date >= '2020-07-16'
	AND b.end_date <= CURDATE()
	AND r.horse_power <= 120 ORDER BY b.total_amount DESC;

    -- CANDIDATE for stored procedure
	SELECT ROUND( SUM(b.total_amount), 2) FROM clients c JOIN bookings b ON c.client_id = b.client_id
	JOIN cars r ON b.car_id = r.car_id
	WHERE b.start_date >= '2020-07-10'
	AND b.end_date <= CURDATE()
	AND r.horse_power <= 120 ORDER BY b.total_amount DESC;

    	-- CANDIDATE for stored procedure
	SELECT ROUND( SUM(b.total_amount), 2) FROM clients c JOIN bookings b ON c.client_id = b.client_id
	JOIN cars r ON b.car_id = r.car_id
	WHERE b.start_date >= '2020-01-01'
	AND b.end_date <= CURDATE()
	AND r.horse_power <= 120 ORDER BY b.total_amount DESC;
