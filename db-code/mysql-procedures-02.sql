
SELECT c.name, b.client_id, b.car_id, b.start_date, c.address FROM bookings b
JOIN clients c ON c.client_id = b.client_id;

SELECT c.city as 'city', b.total_amount as 'amount', c.client_id, r.car_id, r.producer, r.model FROM clients c JOIN bookings b ON c.client_id = b.client_id
JOIN cars r ON b.car_id = r.car_id
WHERE b.start_date >= '2020-07-12'
AND b.end_date <= '2020-07-20'
AND r.horse_power <= 120
ORDER BY b.total_amount DESC;

-- CANDIDATE for stored procedure
SELECT SUM(b.total_amount) FROM clients c JOIN bookings b ON c.client_id = b.client_id
JOIN cars r ON b.car_id = r.car_id
WHERE b.start_date >= '2020-07-12'
AND b.end_date <= '2020-07-20'
AND r.horse_power <= 120
ORDER BY b.total_amount DESC;

-- Commits and Rollbacks
SELECT * FROM bookings WHERE total_amount BETWEEN 1000 AND 2555;

SET autocommit=0;
START TRANSACTION;
UPDATE bookings SET END_DATE=CURDATE(), total_amount=(total_amount +2) WHERE total_amount BETWEEN 1000 AND 2555;
SELECT * FROM bookings WHERE total_amount;
rollback;
