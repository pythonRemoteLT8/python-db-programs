SELECT * FROM car_rental.bookings;

START TRANSACTION;
UPDATE bookings SET total_amount=total_amount-4000 WHERE booking_id = 17;
UPDATE bookings SET total_amount=total_amount+4000 WHERE booking_id = 2222;
-- FAIL transaction
SELECT * FROM bookings WHERE booking_id = 17;

SET AUTOCOMMIT=0;


START TRANSACTION;
DELETE FROM bookings WHERE booking_id = 17;
ROLLBACK;
SELECT * FROM bookings WHERE booking_id = 17;
COMMIT;

START TRANSACTION;
DELETE FROM bookings WHERE booking_id = 1222;
COMMIT;