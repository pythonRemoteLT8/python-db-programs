CREATE DATABASE car_rental;
USE car_rental;

CREATE TABLE cars
(
    car_id INTEGER PRIMARY KEY,
    producer VARCHAR(30),
    model VARCHAR(30),
    year INTEGER,
    horse_power INTEGER,
    price_per_day INTEGER
);

CREATE TABLE clients
(
    client_id INTEGER PRIMARY KEY,
    name VARCHAR(30),
    surname VARCHAR(30),
    address TEXT,
    city VARCHAR(30)
);

CREATE TABLE bookings
(
    booking_id INTEGER PRIMARY KEY,
    client_id INTEGER,
    car_id INTEGER,
    start_date DATE,
    end_date DATE,
    total_amount INTEGER
);

ALTER TABLE clients MODIFY COLUMN client_id INTEGER AUTO_INCREMENT;
ALTER TABLE cars MODIFY COLUMN car_id INTEGER AUTO_INCREMENT;
ALTER TABLE bookings MODIFY COLUMN booking_id INTEGER AUTO_INCREMENT;

ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id),
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id);

INSERT INTO clients (name, surname, address, city) VALUES
('John', 'Smith', 'Wall Street 12', 'New York'),
('Andrew', 'Scott', 'Queen Victoria Street 1', 'London' );
INSERT INTO cars (producer, model, year, horse_power, price_per_day) VALUES
('Seat', 'Leon', 2016, 80, 200),
('Toyota', 'Avensis', 2014, 72, 100);
INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES
(1, 2, '2020-07-05', '2020-07-06', 100),
(2, 2, '2020-07-10', '2020-07-12', 200);

ALTER TABLE clients
    MODIFY COLUMN name VARCHAR(30) NOT NULL,
    MODIFY COLUMN surname VARCHAR(30) NOT NULL,
    MODIFY COLUMN address TEXT NOT NULL,
    MODIFY COLUMN city VARCHAR(30) NOT NULL;

ALTER TABLE cars
    MODIFY COLUMN producer VARCHAR(30) NOT NULL,
    MODIFY COLUMN model VARCHAR(30) NOT NULL,
    MODIFY COLUMN year INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN horse_power INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN price_per_day INTEGER UNSIGNED NOT NULL;

SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE bookings
    MODIFY COLUMN car_id INTEGER NOT NULL,
    MODIFY COLUMN client_id INTEGER NOT NULL,
    MODIFY COLUMN start_date DATE NOT NULL,
    MODIFY COLUMN end_date DATE NOT NULL,
    MODIFY COLUMN total_amount INTEGER UNSIGNED NOT NULL;

 SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO clients (name, surname, address, city) VALUES
    ('Michal', 'Taki', 'os. Srodkowe 12', 'Poznan'),
    ('Pawel', 'Ktory', 'ul. Stara 11', 'Gdynia'),
    ('Anna', 'Inna', 'os. Srednie 1', 'Gniezno'),
    ('Alicja', 'Panna', 'os. Duze 33', 'Torun'),
    ('Damian', 'Papa', 'ul. Skosna 66', 'Warszawa'),
    ('Marek', 'Troska', 'os. Male 90', 'Radom'),
    ('Jakub', 'Klos', 'os. Polskie 19', 'Wadowice'),
    ('Lukasz', 'Lis', 'os. Podlaskie 90', 'Bialystok');

INSERT INTO cars (producer, model, year, horse_power, price_per_day) VALUES
    ('Mercedes', 'CLK', 2018, 190, 400),
    ('Hyundai', 'Coupe', 2014, 165, 300),
    ('Dacia', 'Logan', 2015, 103, 150),
    ('Saab', '95', 2012, 140, 140),
    ('BMW', 'E36', 2007, 110, 80),
    ('Fiat', 'Panda', 2016, 77, 190),
    ('Honda', 'Civic', 2019, 130, 360),
    ('Volvo', 'XC70', 2013, 180, 280);

INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES
    (3, 3, '2020-07-06', '2020-07-08', 400),
    (6, 10, '2020-07-10', '2020-07-16', 1680),
    (4, 5, '2020-07-11', '2020-07-14', 450),
    (5, 4, '2020-07-11', '2020-07-13', 600),
    (7, 3, '2020-07-12', '2020-07-14', 800),
    (5, 7, '2020-07-14', '2020-07-17', 240),
    (3, 8, '2020-07-14', '2020-07-16', 380),
    (5, 9, '2020-07-15', '2020-07-18', 1080),
    (6, 10, '2020-07-16', '2020-07-20', 1120),
    (8, 1, '2020-07-16', '2020-07-19', 600),
    (9, 2, '2020-07-16', '2020-07-21', 500),
    (10, 6, '2020-07-17', '2020-07-19', 280),
    (1, 9, '2020-07-17', '2020-07-19', 720),
    (3, 7, '2020-07-18', '2020-07-21', 240),
    (5, 4, '2020-07-18', '2020-07-22', 1200);


SELECT AVG(c.price_per_day) FROM cars c, bookings b WHERE b.car_id = c.car_id AND b.start_date BETWEEN
'2020-07-01' AND '2020-08-01' AND c.producer = 'Seat' OR c.producer = 'Toyota';

SELECT SUM(c.price_per_day) FROM cars c, bookings b WHERE b.car_id = c.car_id AND b.start_date BETWEEN
'2020-07-01' AND '2020-08-01' AND c.producer = 'Seat' OR c.producer = 'Toyota';

SELECT MIN(c.price_per_day) FROM cars c, bookings b WHERE b.car_id = c.car_id AND b.start_date BETWEEN
'2020-07-01' AND '2020-08-01' AND c.producer = 'Seat' OR c.producer = 'Toyota';

SELECT MAX(c.price_per_day) FROM cars c, bookings b WHERE b.car_id = c.car_id AND b.start_date BETWEEN
'2020-07-01' AND '2020-08-01' AND c.producer = 'Seat' OR c.producer = 'Toyota';

SELECT COUNT(*) FROM cars c, bookings b WHERE b.car_id = c.car_id AND b.start_date BETWEEN
'2020-07-01' AND '2020-08-01' AND c.producer = 'Seat' OR c.producer = 'Toyota';

SELECT COUNT(*) FROM bookings b;

SELECT c.producer, SUM( b.total_amount ) AS "Total sum", COUNT( b.total_amount ) AS "Count of bookings", ROUND( AVG( b.total_amount ), 2)
AS "Average of" FROM bookings b, cars c  WHERE b.car_id = c.car_id
AND b.start_date BETWEEN '2020-07-01' AND '2020-08-01' AND c.producer = 'Mercedes' OR c.producer = 'Toyota'
GROUP BY c.producer;

SELECT c.producer, SUM( b.total_amount ) FROM bookings b, cars c  WHERE b.car_id = c.car_id
AND b.start_date BETWEEN '2020-07-01' AND '2020-08-01' AND c.producer = 'Mercedes' OR c.producer = 'Toyota' OR c.producer = 'Honda'
GROUP BY c.producer HAVING COUNT( b.booking_id ) < 10;

-- From July to August -- group by maker -- count all totals -- but select the ones -- with total amount average as less than 500
SELECT c.producer, SUM( b.total_amount ), GROUP_CONCAT( b.total_amount ) FROM bookings b, cars c  WHERE b.car_id = c.car_id
AND b.start_date BETWEEN '2020-07-01' AND '2020-08-01' AND c.producer = 'Mercedes' OR c.producer = 'Toyota' OR c.producer = 'Honda'
GROUP BY c.producer HAVING AVG( b.total_amount ) > 500;

SELECT client_id, surname, name FROM clients WHERE surname LIKE "P%" AND name LIKE "%an";

SELECT c.name, c.surname, b.total_amount FROM bookings b JOIN clients c ON c.client_id = b.client_id
WHERE b.total_amount > 1000;

-- Tripple join to identify best earning -city
SELECT c.city, b.total_amount FROM clients c JOIN bookings b ON c.client_id = b.client_id
JOIN cars r ON b.car_id = r.car_id
WHERE b.start_date >= '2020-07-12' AND b.end_date <= '2020-07-20'
AND r.horse_power <= 120 ORDER BY b.total_amount DESC LIMIT 1;

SELECT r.producer, COUNT(b.car_id), GROUP_CONCAT( r.horse_power ) FROM bookings b JOIN cars r ON b.car_id = r.car_id
WHERE r.price_per_day >= 300 GROUP BY r.horse_power, r.producer
ORDER BY r.horse_power DESC;

SELECT SUM(total_amount) FROM bookings WHERE start_date >= '2020-07-14' AND end_date <= '2020-07-18';

-- Find bookings of average total_amount for aggregated number of cars > 2
SELECT ROUND( AVG(b.total_amount),2 ) AS Average_booking_value,
COUNT(b.car_id) AS Number_of_cars, c.city, c.address,
c.name AS Name, c.surname AS Surname
FROM bookings b
JOIN clients c ON c.client_id = b.client_id
GROUP BY b.client_id, c.address
HAVING Number_of_cars >= 2
ORDER BY Number_of_cars DESC;

SELECT SUM(total_amount)  AS 'Amount total' FROM bookings b WHERE b.booking_id <> 3 AND b.start_date >= '2020-07-14' AND b.end_date <= '2020-07-18';

select c.producer,
count(c.car_id),
sum(b.total_amount) total_sum,
avg(b.total_amount) average
from bookings b
join cars c on c.car_id = b.car_id
where b.start_date between '2020-07-01' and '2020-08-01'
and (c.producer = 'Toyoata' or c.producer = 'Mercedes')
group by c.producer
having count(b.booking_id)<10;

SELECT b.car_id, c.car_id, b.start_date FROM cars c, bookings b LEFT JOIN cars ON b.car_id=cars.car_id ;

SELECT * FROM bookings b JOIN cars c ON b.car_id=c.car_id WHERE b.booking_id = 11 AND c.car_id = 10;

SELECT COUNT(DISTINCT clients.name) FROM clients;