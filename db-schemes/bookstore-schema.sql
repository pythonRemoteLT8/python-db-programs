CREATE DATABASE all_books;

USE all_books;

CREATE TABLE Book(
    ISBN VARCHAR(20),
    Publisher VARCHAR(30),
    Title VARCHAR(40) UNIQUE,
    PRIMARY KEY(ISBN, Title)
);

INSERT INTO Book (ISBN,Publisher,Title) VALUES ("HP-304-34921", "Waterhouse", "Harry Potter I" );

INSERT INTO Book (ISBN,Publisher,Title) VALUES ("HP-305-34922", "Lilies", "Harry Potter II" );

INSERT INTO Book SET ISBN="HP-306-34923", Publisher='Waterstone', Title='Harry Potter III';

INSERT INTO Book (ISBN,Title) VALUES ("OLD-9433", "Incunabulus Codex" );

CREATE INDEX isbn_index ON Book (ISBN);

CREATE INDEX index_title ON Book (Title, Publisher);

-- SELECT * FROM Book;

SHOW INDEX FROM Book;

SHOW COLUMNS FROM all_books.Book LIKE 'Publish%';

ALTER TABLE Book ADD INDEX isbn_index(ISBN);

ALTER TABLE Book ALTER COLUMN Publisher SET DEFAULT 'unknown';


