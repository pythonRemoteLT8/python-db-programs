CREATE DATABASE  IF NOT EXISTS `cinematic` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `cinematic`;
-- MySQL dump 10.13  Distrib 8.0.28, for Linux (x86_64)
--
-- Host: localhost    Database: cinematic
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `directors`
--

DROP TABLE IF EXISTS `directors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `directors` (
  `director_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `surname` varchar(45) NOT NULL,
  `rating` int DEFAULT NULL,
  PRIMARY KEY (`director_id`),
  UNIQUE KEY `uniq_director_idx` (`name`,`surname`),
  UNIQUE KEY `idx_directors_name_surname` (`name`,`surname`) COMMENT 'Do not duplicate directors'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `directors`
--

LOCK TABLES `directors` WRITE;
/*!40000 ALTER TABLE `directors` DISABLE KEYS */;
INSERT INTO `directors` VALUES (1,'Frank','Darabont',7),(2,'Francis Ford','Coppola',8),(3,'Quentin','Tarantino',10),(4,'Christopher','Nolan',9),(5,'David','Fincher',7),(7,'Algimantas','Puipa',6),(8,'Vytautas','Žalakevičius',7),(9,'Kristijonas','Vildžiūnas',9);
/*!40000 ALTER TABLE `directors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movies` (
  `movie_id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `year` int NOT NULL,
  `category` varchar(45) NOT NULL,
  `director_id` int NOT NULL,
  `rating` int DEFAULT NULL,
  PRIMARY KEY (`movie_id`),
  UNIQUE KEY `uniq_movies_idx` (`title`,`year`,`category`),
  UNIQUE KEY `idx_movies_title_year_director_id` (`title`,`year`,`director_id`) COMMENT 'Do not duplicate movies',
  KEY `FK_directors_movies` (`director_id`),
  CONSTRAINT `FK_directors_movies` FOREIGN KEY (`director_id`) REFERENCES `directors` (`director_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,'The Shawshank Redemption',1994,'Drama',1,8),(2,'The Green Mile',1999,'Drama',1,6),(3,'The Godfather',1972,'Crime',2,7),(4,'The Godfather III',1990,'Crime',2,6),(5,'Pulp Fiction',1994,'Crime',3,9),(6,'Inglourious Basterds',2009,'War',3,8),(7,'The Dark Knight',2008,'Action',4,9),(8,'Interstellar',2014,'Sci-fi',4,8),(9,'The Prestige',2006,'Drama',4,10),(10,'Fight Club',1999,'Drama',5,7),(11,'Zodiac',2007,'Crime',5,5),(12,'Seven',1995,'Drama',5,8),(13,'Alien 3',1992,'Horror',5,5);
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'cinematic'
--

--
-- Dumping routines for database 'cinematic'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-04-09 14:18:26
