
SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS cars;
CREATE TABLE cars
(
    car_id INTEGER PRIMARY KEY,
    producer VARCHAR(30),
    model VARCHAR(30),
    year INTEGER,
    horse_power INTEGER,
    price_per_day INTEGER
) ENGINE=InnoDB;

DROP TABLE IF EXISTS clients;
CREATE TABLE clients
(
    client_id INTEGER PRIMARY KEY,
    name VARCHAR(30),
    surname VARCHAR(30),
    address TEXT,
    city VARCHAR(30)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS bookings;
CREATE TABLE bookings
(
    booking_id INTEGER PRIMARY KEY,
    client_id INTEGER,
    car_id INTEGER,
    start_date DATE,
    end_date DATE,
    total_amount INTEGER
) ENGINE=InnoDB;

DROP TABLE IF EXISTS authors;
SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE clients MODIFY COLUMN client_id INTEGER AUTO_INCREMENT;
ALTER TABLE cars MODIFY COLUMN car_id INTEGER AUTO_INCREMENT;
ALTER TABLE bookings MODIFY COLUMN booking_id INTEGER AUTO_INCREMENT;

ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id),
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id);

-- For CASCADE delete
ALTER TABLE bookings
DROP FOREIGN KEY `client_id_fk`,
DROP FOREIGN KEY `car_id_fk`;

-- Recreate the same FKs with DELETE CASCADE
ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id) ON DELETE CASCADE;
commit;

ALTER TABLE clients
    MODIFY COLUMN name VARCHAR(30) NOT NULL,
    MODIFY COLUMN surname VARCHAR(30) NOT NULL,
    MODIFY COLUMN address TEXT NOT NULL,
    MODIFY COLUMN city VARCHAR(30) NOT NULL;

ALTER TABLE cars
    MODIFY COLUMN producer VARCHAR(30) NOT NULL,
    MODIFY COLUMN model VARCHAR(30) NOT NULL,
    MODIFY COLUMN year INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN horse_power INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN price_per_day INTEGER UNSIGNED NOT NULL;

SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE bookings
    MODIFY COLUMN car_id INTEGER NOT NULL,
    MODIFY COLUMN client_id INTEGER NOT NULL,
    MODIFY COLUMN start_date DATE NOT NULL,
    MODIFY COLUMN end_date DATE NOT NULL,
    MODIFY COLUMN total_amount INTEGER UNSIGNED NOT NULL;

 SET FOREIGN_KEY_CHECKS = 1;

commit;
SELECT * FROM bookings b, cars c WHERE c.car_id = b.car_id;

DELETE FROM cars WHERE car_id =1;
commit;

SET FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS cars;
CREATE TABLE cars
(
    car_id INTEGER PRIMARY KEY,
    producer VARCHAR(30),
    model VARCHAR(30),
    year INTEGER,
    horse_power INTEGER,
    price_per_day INTEGER
) ENGINE=InnoDB;

DROP TABLE IF EXISTS clients;
CREATE TABLE clients
(
    client_id INTEGER PRIMARY KEY,
    name VARCHAR(30),
    surname VARCHAR(30),
    address TEXT,
    city VARCHAR(30)
) ENGINE=InnoDB;

DROP TABLE IF EXISTS bookings;
CREATE TABLE bookings
(
    booking_id INTEGER PRIMARY KEY,
    client_id INTEGER,
    car_id INTEGER,
    start_date DATE,
    end_date DATE,
    total_amount INTEGER
) ENGINE=InnoDB;

DROP TABLE IF EXISTS authors;
SET FOREIGN_KEY_CHECKS = 1;

ALTER TABLE clients MODIFY COLUMN client_id INTEGER AUTO_INCREMENT;
ALTER TABLE cars MODIFY COLUMN car_id INTEGER AUTO_INCREMENT;
ALTER TABLE bookings MODIFY COLUMN booking_id INTEGER AUTO_INCREMENT;

ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id),
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id);

-- For CASCADE delete
ALTER TABLE bookings
DROP FOREIGN KEY `client_id_fk`,
DROP FOREIGN KEY `car_id_fk`;

-- Recreate the same FKs with DELETE CASCADE
ALTER TABLE bookings
ADD CONSTRAINT client_id_fk FOREIGN KEY (client_id) REFERENCES clients (client_id) ON DELETE CASCADE,
ADD CONSTRAINT car_id_fk FOREIGN KEY (car_id) REFERENCES cars (car_id) ON DELETE CASCADE;
commit;

ALTER TABLE clients
    MODIFY COLUMN name VARCHAR(30) NOT NULL,
    MODIFY COLUMN surname VARCHAR(30) NOT NULL,
    MODIFY COLUMN address TEXT NOT NULL,
    MODIFY COLUMN city VARCHAR(30) NOT NULL;

ALTER TABLE cars
    MODIFY COLUMN producer VARCHAR(30) NOT NULL,
    MODIFY COLUMN model VARCHAR(30) NOT NULL,
    MODIFY COLUMN year INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN horse_power INTEGER UNSIGNED NOT NULL,
    MODIFY COLUMN price_per_day INTEGER UNSIGNED NOT NULL;

SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE bookings
    MODIFY COLUMN car_id INTEGER NOT NULL,
    MODIFY COLUMN client_id INTEGER NOT NULL,
    MODIFY COLUMN start_date DATE NOT NULL,
    MODIFY COLUMN end_date DATE NOT NULL,
    MODIFY COLUMN total_amount INTEGER UNSIGNED NOT NULL;

 SET FOREIGN_KEY_CHECKS = 1;

 INSERT INTO clients (name, surname, address, city) VALUES
('John', 'Smith', 'Wall Street 12', 'New York'),
('Andrew', 'Scott', 'Queen Victoria Street 1', 'London' );
INSERT INTO cars (producer, model, year, horse_power, price_per_day) VALUES
('Seat', 'Leon', 2016, 80, 200),
('Toyota', 'Avensis', 2014, 72, 100);
INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES
(1, 2, '2020-07-05', '2020-07-06', 100),
(2, 2, '2020-07-10', '2020-07-12', 200);

INSERT INTO clients (name, surname, address, city) VALUES
    ('Michal', 'Taki', 'os. Srodkowe 12', 'Poznan'),
    ('Pawel', 'Ktory', 'ul. Stara 11', 'Gdynia'),
    ('Anna', 'Inna', 'os. Srednie 1', 'Gniezno'),
    ('Alicja', 'Panna', 'os. Duze 33', 'Torun'),
    ('Damian', 'Papa', 'ul. Skosna 66', 'Warszawa'),
    ('Marek', 'Troska', 'os. Male 90', 'Radom'),
    ('Jakub', 'Klos', 'os. Polskie 19', 'Wadowice'),
    ('Lukasz', 'Lis', 'os. Podlaskie 90', 'Bialystok');

INSERT INTO cars (producer, model, year, horse_power, price_per_day) VALUES
    ('Mercedes', 'CLK', 2018, 190, 400),
    ('Hyundai', 'Coupe', 2014, 165, 300),
    ('Dacia', 'Logan', 2015, 103, 150),
    ('Saab', '95', 2012, 140, 140),
    ('BMW', 'E36', 2007, 110, 80),
    ('Fiat', 'Panda', 2016, 77, 190),
    ('Honda', 'Civic', 2019, 130, 360),
    ('Volvo', 'XC70', 2013, 180, 280);

INSERT INTO bookings (client_id, car_id, start_date, end_date, total_amount) VALUES
    (3, 3, '2020-07-06', '2020-07-08', 400),
    (6, 10, '2020-07-10', '2020-07-16', 1680),
    (4, 5, '2020-07-11', '2020-07-14', 450),
    (5, 4, '2020-07-11', '2020-07-13', 600),
    (7, 3, '2020-07-12', '2020-07-14', 800),
    (5, 7, '2020-07-14', '2020-07-17', 240),
    (3, 8, '2020-07-14', '2020-07-16', 380),
    (5, 9, '2020-07-15', '2020-07-18', 1080),
    (6, 10, '2020-07-16', '2020-07-20', 1120),
    (8, 1, '2020-07-16', '2020-07-19', 600),
    (9, 2, '2020-07-16', '2020-07-21', 500),
    (10, 6, '2020-07-17', '2020-07-19', 280),
    (1, 9, '2020-07-17', '2020-07-19', 720),
    (3, 7, '2020-07-18', '2020-07-21', 240),
    (5, 4, '2020-07-18', '2020-07-22', 1200);

commit;
SELECT * FROM bookings b, cars c WHERE c.car_id = b.car_id;

DELETE FROM cars WHERE car_id =2;
commit;

SELECT * FROM bookings b, cars c WHERE c.car_id = b.car_id AND c.car_id = 2;

SELECT * FROM bookings b WHERE b.car_id = 2;
-- NOTE: no car #2 in CARS nor in BOOKINS!
