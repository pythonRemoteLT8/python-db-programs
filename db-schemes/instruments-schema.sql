
SELECT VERSION();

SHOW DATABASES;

CREATE TABLE instruments(
    instrument_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    family VARCHAR(30) NOT NULL,
    difficulty ENUM('easy', 'medium', 'hard') NOT NULL
);


SELECT * FROM music.instruments;

SELECT family, count(*) as count FROM instruments GROUP BY family;

-- TRUNCATE instruments;