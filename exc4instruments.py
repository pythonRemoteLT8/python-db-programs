
import MySQLdb

from MySQLdb import DatabaseError
import logging

user = 'root'
password = '1My_sql!SQL'

db = MySQLdb.connect(host="localhost", user=user, passwd=password, db='music')

create_tables = """
CREATE TABLE IF NOT EXISTS instruments(
    instrument_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    family VARCHAR(30) NOT NULL,
    difficulty ENUM('easy', 'medium', 'hard') NOT NULL
);
"""

def insert_instruments(connection, instrument_values):
    if not instrument_values:
        return


    truncate_sql = """ 
    TRUNCATE instruments
    """
    connection.cursor().execute(truncate_sql)
    connection.commit()

    instrument_set = [('harp%d' % 0, 'strings', 'hard')]

    for idx in range( 1, 10 ):

        small_instrument_set = ('harp%d'%idx, 'strings', 'hard')

        instrument_set.append( small_instrument_set );

        insert_sql = """
        INSERT INTO instruments (name, family, difficulty) VALUES(%s, %s, %s)
        """
        connection.cursor().executemany(insert_sql, instrument_set )

        print( "Short cursor: %s" % connection.cursor() )

    connection.commit()

def get_instruments_count(connection):
    get_count_sql = """SELECT family, count(*) as count FROM instruments GROUP BY family;"""
    cursor = connection.cursor(MySQLdb.cursors.DictCursor)
    cursor.execute(get_count_sql)

    print("Procedure cursor: %s" % cursor)
    return cursor.fetchall()

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

try:

    # ZeroDivisionError: division
    # error_output = 1/0;

    cursor = db.cursor()
    print("Main program cursor: %s" % cursor)

    cursor.execute(create_tables)
    print( cursor )
    db.commit()

    instruments = [
        ('guitar', 'strings', 'medium'),
        ('piano', 'keyboard', 'hard'),
        ('harp', 'strings', 'hard'),
        ('triangle', 'percussion', 'easy'),
        ('flute', 'woodwind', 'medium'),
        ('violin', 'strings', 'medium'),
        ('tambourine', 'percussion', 'easy'),
        ('organ', 'keyboard', 'hard')]

    with db:
        insert_instruments(db, instruments)
        logger.info('DB OK: ' + " added)" )
        print ( instruments )

        result = get_instruments_count(db)
        print( "%d instrument GROUPS in DB" %  len(result) )
        print( result[:] )

#except Exception, e:
#similarly:

except DatabaseError as error:
    logger.fatal('DB Exception Error: %s' % error )

except ArithmeticError as error:
    logger.warning('ArithmeticError Exception Error: %s' % error )

except (BaseException, TypeError) as error:
    help( error )
    logger.info('Base Exception Error: %s' % error )

except (RuntimeError, NameError):
    logger.error('Generic Exception Error.' )
